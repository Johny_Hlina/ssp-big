"""
Simple python code for exploring how syntax checking and pylint works
"""


def lin_seq(nth_term):
    """
    generate linear sequence to nth term
    """
    if nth_term<0:
        return None
    seq = []
    for i in range(nth_term):
        seq.append(i)
    return seq

def square_seq(nth_term):
    """
    generate quadratic sequence to nth term
    """
    if nth_term<0:
        return None
    seq = []
    for i in range(nth_term):
        seq.append(i**2)
    return seq

def cubic_seq(nth_term):
    """
    generate cubic sequence to nth term
    """
    if nth_term<0:
        return None
    seq = []
    for i in range(nth_term):
        seq.append(i**3)
    return seq

def fib_seq(nth_term):
    """
    generate fibonacci sequence to nth term
    """
    if nth_term<0:
        return None
    n_1, n_2 = 0, 1
    count = 0
    seq = []
    while count < nth_term:
        seq.append(n_2)
        nth = n_1 + n_2
        n_1 = n_2
        n_2 = nth
        count += 1
    return seq
